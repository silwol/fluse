#![deny(missing_docs)]

//! An interface for linting tools
//!
//! This is experimental work in progress, expect breaking changes!

/// The outcome of a check.
pub enum Outcome<'a> {
    /// The check passed.
    Passed(&'a str),
    /// The check failed.
    Failed(&'a str),
    /// The check was ignored.
    Ignored,
}

impl<'a> Outcome<'a> {
    /// Check whether the outcome is failed.
    pub fn is_failed(&self) -> bool {
        use Outcome::*;
        match *self {
            Passed(_) | Ignored => false,
            Failed(_) => true,
        }
    }
}

/// A summary giving short information about a check.
#[derive(Debug, PartialEq)]
pub enum Summary {
    /// The check was ok, no failures were found.
    Ok,
    /// At least one failure was found.
    NotOk,
}

impl Summary {
    /// Check whether the summary is ok.
    pub fn is_ok(&self) -> bool {
        *self == Summary::Ok
    }
}

/// A trait for checks that can be implemented.
pub trait Check {
    /// Runs the check.
    ///
    /// This is the only method that is required by implementors.
    ///
    /// The implementation should call f for each check that was run,
    /// so that the caller gets notified about the outcome.
    fn check(&self, f: &mut FnMut(&Outcome));

    /// Run the check and return a summary.
    ///
    /// This method runs the check and records the calls to the f
    /// parameter, collecting a summary over all of them.
    fn check_ok(&self, f: &mut FnMut(&Outcome)) -> Summary {
        let mut ok = Summary::Ok;
        self.check(&mut |o| {
            if o.is_failed() {
                ok = Summary::NotOk;
            }
            f(o)
        });
        ok
    }
}
